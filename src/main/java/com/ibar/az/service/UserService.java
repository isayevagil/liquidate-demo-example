package com.ibar.az.service;

import com.ibar.az.dto.UserDTO;
import com.ibar.az.model.User;

public interface UserService {
    UserDTO createUserD(User user);
}
