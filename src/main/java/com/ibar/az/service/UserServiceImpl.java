package com.ibar.az.service;

import com.ibar.az.dto.UserDTO;
import com.ibar.az.model.User;
import com.ibar.az.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service("userService")
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public UserDTO createUserD(User user) {
        userRepository.save (user);
        return UserDTO.builder ().name (user.getName ()).surname (user.getSurname ()).build ();
    }
}
