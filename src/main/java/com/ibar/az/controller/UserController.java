package com.ibar.az.controller;

import com.ibar.az.dto.UserDTO;
import com.ibar.az.model.User;
import com.ibar.az.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/save")
    public UserDTO saveUser(@RequestBody User user) {
        return userService.createUserD (user);
    }
}
